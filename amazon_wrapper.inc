<?php
//
// A simple wrapper for the complicated inner workings of Prometheus6's excellent amazontools.module.
// Rather than assuming that products are always associated with nodes, this set of functions treats
// the {amazonitem} like a cache.
//
// This approach makes it easier for helper modules to utilize the data even when full-fledged nodes
// are unecessary or unwanted. Since amazon_item and amazon node types are joined to it via asin
// rather than nid, it doesn't hurt to have extra {amazonitem} cache records around.

function amazon_get_for_asin($asin = '', $force_lookup = FALSE, $cache = TRUE) {
  $amazon_items = amazon_get_for_asins(array($asin), $force_lookup, $cache);
  if (count($amazon_items)) {
    return $amazon_items[$asin];
  }
  return NULL;
}

function amazon_get_for_asins($asins = array(), $force_lookup = FALSE, $cache = TRUE) {
  $amazon_items = array();
  $to_look_up = $asins;

  if ($force_lookup) {
    $asins = array();
  }

  foreach(amazon_get_from_cache($asins) as $item) {
    $amazon_items[$item->asin] = $item;
    $to_look_up = array_diff($to_look_up, array_keys($amazon_items));
  }
  
  foreach(amazon_get_from_web($to_look_up, $cache) as $item) {
    $amazon_items[$item->asin] = $item;
  }

  return $amazon_items;
}

function amazon_get_from_web($asins = array(), $cache = TRUE) {
  $amazon_items = array();
  if (count($asins)) {
    $items = _amazon_product_data_from_Amazon(implode($asins, ','));
    if ($cache) {
      foreach($items as $item) {
        $amazon_items[$amazon_item->asin] = $item;
        amazon_cache_item($item);
      }
    }
  }
  return $amazon_items;
}

function amazon_get_from_cache($asins = array()) {
  $amazon_items = array();
  if (count($asins)) {
    foreach($asins as $asin) {
      $item = _amazon_product_db_data($asin);
      $amazon_items[$item->asin] = $item;
    }
  }
  return $amazon_items;
}

function amazon_cache_item($item) {
  if ($item->asin != '') {
    // first, wipe out any past cached amazon records.
    db_query("DELETE FROM {amazonitem} WHERE asin='%s'", $item->asin);

    // now, insert the new stuff.
    db_query("INSERT INTO {amazonitem} (asin, detailpageurl,
       smallimageurl, smallimageheight, smallimagewidth,
       mediumimageurl, mediumimageheight, mediumimagewidth,
       largeimageurl, largeimageheight, largeimagewidth,
       author, editorialreview, binding, listamount,
       listcurrencycode, listformattedprice, title,
       amount, currencycode, formattedprice, pricedate, availability)
       VALUES ('%s', '%s',
       '%s', %d, %d,
       '%s', %d, %d,
       '%s', %d, %d,
       '%s', '%s', '%s', %d,
       '%s', '%s', '%s',
       %d, '%s', '%s',
       '%s', '%s')",
       $item->asin, $item->detailpageurl,
       db_escape_string($item->smallimageurl), $item->smallimageheight, $item->smallimagewidth,
       db_escape_string($item->mediumimageurl), $item->mediumimageheight, $item->mediumimagewidth,
       db_escape_string($item->largeimageurl), $item->largeimageheight, $item->largeimagewidth,
       serialize($item->author), db_escape_string($item->editorialreview), $item->binding,
       $item->listamount,
       $item->listcurrencycode, $item->listformattedprice, $item->title,
       $item->amount ? $item->amount: $item->listamount, $item->currencycode,
       $item->formattedprice ? $item->formattedprice: $item->listformattedprice,
       date('Y-m-d H:i:s'), $item->availability);
  }
}
